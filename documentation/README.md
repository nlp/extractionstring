# Documentation of `extractionstring` Python package

The documentation is available on [https://nlp.frama.io/extractionstring/](https://nlp.frama.io/extractionstring/)

The PyPi package is available on [https://pypi.org/project/extractionstring/](https://pypi.org/project/extractionstring/)

The official repository is on [https://framagit.org/nlp/extractionstring/](https://framagit.org/nlp/extractionstring/)

## Summary

The `ExtractionString` class represents the extraction of sub-string from a parent string. As such, it is usefull for the [tokenization](https://en.wikipedia.org/wiki/Lexical_analysis#Tokenization) or [chunking / shallow parsing](https://en.wikipedia.org/wiki/Shallow_parsing), in addition to annotating a text, extracting information, ..., In fact, it is usefull for any prerequisite of natural language processing (NLP) tasks that use selecting sub-parts of a parent string as starting point.

[Compared with other libraries](tutorials/comparison_other_libraries), the `ExtractionString` package is based on mathematical aspects of the string. It thus encompass beautifully the basic aspects of selection that other libraries offer too. In addition, it offers the versatility of the mathematical modelization of a collection of sets of positions attached to a parent string, that represent the children strings. As such, `ExtractionString` is the unique universal representation of sub-string extraction from a parent string. Associated to its brother package, the `SubstitutionString` library that allows versatile manipulation of string changes, these two packages form the basics for a universal preprocessing representation of NLP treatments.

## `EvenSizedSortedSet` class

The [`EvenSizedSortedSet` class](userguides/EvenSizedSortedSet_UserGuide) corresponds to the abstraction of a collection of non-overlapping intervals, characterized by sorted integers representing the start and stop positions of the intervals, so these integers come in pairs and there is an even number of integers in this collection of sorted integers. In addition, the basic algebra of set of positions (union, difference, intersection, symmetric difference) is available at this level, and several ordering procedures are available for such collection of sorted integers of even size.

## `ExtractionString` class

The `ExtractionString` class are kind of containers usefull to construct elaborated tokenizers or chunkers. It is basically the gluing of an `EvenSizedSortedSet` on top of a string, called a parent-string in that context, while the extraction themselves are called the children-strings.

The [`ExtractionString` complete user guide](userguides/ExtractionString_3_Complete_UserGuide) presents all methods available for these two classes.

The `ExtractionString` class are explained in a series of documents, either in Markdown or Jupyter-Notebook format: 

 - [ExtractionString: QuickStart](userguides/ExtractionString_quickstart): gives a rapid tour on the `ExtractionString` possibilities, and serves as basic illustration of it usefullness.
 - [ExtractionString: Motivation](userguides/ExtractionString_0_Motivation_UserGuide) : explores the motivations behind the construction of the objects
 - [ExtractionString: BasicExample](userguides/ExtractionString_1_BasicExample_UserGuide) : explores the basic usage of the a unique contiguous interval underneath the parent string, and the relative (position inside the children string) to absolute (position inside the parent string) coordinate systems, in addition to the non-destruction of information with selecting sub-intervals of the parent string that constitute the children-strings. 
 - [ExtractionString: MultipleIntervals](userguides/ExtractionString_2_MultipleIntervals_UserGuide) : explores the novelties of this library, represented by the juxtapositions of non-overlapping sub-intervals of the parent string positions, their ordering algebra of extraction, and set algebra of union, intersection, difference and symmetric difference
 - [ExtractionSTring_completeAPI](api/extractionstring.extraction_string) : Complete API description of the class.

## Advanced Tokenizers

There are several more elaborated tokenizer constructed from the `ExtractionString` class. Those have been displaced to a specific package, called `iamtokenizing`, and available on https://pypi.org/project/iamtokenizing/.
