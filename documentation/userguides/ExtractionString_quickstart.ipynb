{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# `ExtractionString` quick start\n",
    "\n",
    "We present the `ExtractionString` class, which lies at the heart of this library. It models the notion of a section of a parent string, and allows algebraic manipulation of different sections taken from the same parent string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from extractionstring import ExtractionString\n",
    "\n",
    "string = 'Simple string for demonstration and for illustration.'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic examples\n",
    "\n",
    "We here illustrate the basic construction of the `ExtractionString` object, then we reconstruct the usual basic tokenizer. note nevertheless that the approach here is not customized, and the use of the companion class `iamtokenizing` is recommended, see [https://framagit.org/nlp/iamtokenizing](https://framagit.org/nlp/iamtokenizing) for more details.\n",
    "\n",
    "### Attributes of the `ExtractionString` class\n",
    "\n",
    "We start with the explicit construction of the above examples of `span1` and `span2`. If one defines no `intervals` attributes while instanciating the `ExtractionString` class, this attribute is calculated from the given parent string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "EvenSizedSortedSet[(0,53)]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span = ExtractionString(string)\n",
    "span.intervals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to construct the `span1`, one can construct the `intervals` attributes manually"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('string for illustration', [(7,13);(36,39);(40,52)])"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span1 = ExtractionString(string, intervals=[7,13, 36,39, 40,52])\n",
    "span1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same for `span2`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('string for illustration', [(7,13);(14,17);(40,52)])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span2 = ExtractionString(string, intervals=[7,13, 14,17, 40,52])\n",
    "span2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In passing, note that the attributes `subtoksep` is used to glue the different sub-strings given by the ranges of `ExtractionString`. By default, `ExtractionString.subtoksep = chr(32)` (the white space of length 1), and it is wise to keep a sub-token separator of length 1, but for illustration we change it here to a more complex pattern"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('string _SubTokSep_ for _SubTokSep_ illustration', [(7,13);(36,39);(40,52)])"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span1_subtoksep = ExtractionString(string, \n",
    "                       intervals=[7,13, 36,39, 40,52],\n",
    "                       subtoksep=' _SubTokSep_ ')\n",
    "span1_subtoksep"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Child-string `str(ExtractionString)` versus parent-string `ExtractionString.string` representations\n",
    "\n",
    "What is captured by the `ExtractionString` representation is the collection of ranges, and its string representation, whereas `ExtractionString.string` keeps the parent string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "EvenSizedSortedSet[(7,13);(36,39);(40,52)]\n",
      "string for illustration\n",
      "Simple string for demonstration and for illustration.\n"
     ]
    }
   ],
   "source": [
    "print(span1.intervals)\n",
    "print(str(span1))\n",
    "print(span1.string)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "EvenSizedSortedSet[(7,13);(36,39);(40,52)]\n",
      "string _SubTokSep_ for _SubTokSep_ illustration\n",
      "Simple string for demonstration and for illustration.\n"
     ]
    }
   ],
   "source": [
    "print(span1_subtoksep.intervals)\n",
    "print(str(span1_subtoksep))\n",
    "print(span1_subtoksep.string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Different levels of equality\n",
    "\n",
    "Since Python refers to the objects via reference, the `ExtractionString.string` is really the same object among the different instances of `ExtractionString` if they are constructed accordingly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span2.string == span1.string"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the same way, since `str(ExtractionString)` is a string, the equality among the two resulting objects is the usual comparison of string"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "str(span1) == str(span2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition, the equality of `ExtractionString` object verifies that `ExtractionString.string` _and_ `ExtractionString.ranges` _and_ `ExtractionString.subtoksep` are the same"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span1 == span2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span1 == span1_subtoksep"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `subExtractionStrings` attributes\n",
    "\n",
    "In case there are several ranges in the `ExtractionString.ranges` attributes, the `subExtractionStrings` parameters constructs the different instances of `ExtractionString` corresponding to the sub-parts, and collect them in a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('string', [(7,13)]),\n",
       " ExtractionString('for', [(36,39)]),\n",
       " ExtractionString('illustration', [(40,52)])]"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span1.extractions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It discards the use of `subtoksep`, ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('string', [(7,13)]),\n",
       " ExtractionString('for', [(36,39)]),\n",
       " ExtractionString('illustration', [(40,52)])]"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span1_subtoksep.extractions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... despite it is still there."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "' _SubTokSep_ '"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span1_subtoksep.extractions[0].subtoksep"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Construction of basic Tokenizer from `ExtractionString`\n",
    "\n",
    "Let us construct the basic tokenizers one usually encounters in [NLP](https://en.wikipedia.org/wiki/Lexical_analysis#Tokenization), namely the n-gram (mobile window of n contiguous words) and char-gram (mobile window of n contiguous characters).\n",
    "\n",
    "### Construction of n-grams\n",
    "\n",
    "To recover the basic n-grams construction, we have to find a way to construct the ranges of useless (or usefull, depending on the strategy of keeping/excluding the sub-strings from the parent string) parts of the parent string. This is done using the [re.finditer](https://docs.python.org/3/library/re.html) matches and the REGular EXpressions. Below we use it on the usefull parts, that is any contiguous portions of alpha-numerics characters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('Simple string for demonstration and for illustration', [(0,6);(7,13);(14,17);(18,31);(32,35);(36,39);(40,52)])"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from re import finditer \n",
    "intervals = []\n",
    "for f in finditer('\\w+', string):\n",
    "    intervals.append(f.start())\n",
    "    intervals.append(f.end())\n",
    "ngrams = ExtractionString(string, intervals)\n",
    "ngrams"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It results a single ExtractionString, since one knows that the REGEX are never overlapping. In order to construct n-grams (below example is 2-grams) that are overlapping sub-tokens of the parent string, one simply has to construct independent `ExtractionString` instance, each of them being attached to the same parent string. This is easy using the `subExtractionStrings` constructor, which does that automatically, and to join the succesive sub-spans using the union operation `+` (that we will deal with later)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('Simple string', [(0,6);(7,13)]),\n",
       " ExtractionString('string for', [(7,13);(14,17)]),\n",
       " ExtractionString('for demonstration', [(14,17);(18,31)]),\n",
       " ExtractionString('demonstration and', [(18,31);(32,35)]),\n",
       " ExtractionString('and for', [(32,35);(36,39)]),\n",
       " ExtractionString('for illustration', [(36,39);(40,52)])]"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ngrams_21 = [s1+s2 for s1, s2 in zip(ngrams.extractions[:-1], ngrams.extractions[1:])]\n",
    "ngrams_21"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If one prefers, one can directly constructs the ranges, and pass them from the beginning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('Simple string', [(0,6);(7,13)]),\n",
       " ExtractionString('string for', [(7,13);(14,17)]),\n",
       " ExtractionString('for demonstration', [(14,17);(18,31)]),\n",
       " ExtractionString('demonstration and', [(18,31);(32,35)]),\n",
       " ExtractionString('and for', [(32,35);(36,39)]),\n",
       " ExtractionString('for illustration', [(36,39);(40,52)])]"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ngrams_22 = [ExtractionString(string, intervals=[r1, r2, r3, r4]) \n",
    "             for r1, r2, r3, r4 in zip(\n",
    "                 intervals[:-3:2], intervals[1:-2:2], \n",
    "                 intervals[2:-1:2], intervals[3::2])]\n",
    "ngrams_22"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The two constructions are equivalent"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "True\n",
      "True\n",
      "True\n",
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "for s1, s2 in zip(ngrams_21, ngrams_22):\n",
    "    print(s1==s2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `ExtractionString.start` and `ExtractionString.stop` parameters\n",
    "\n",
    "One might be unsatisfied by the presence of discontiguous ranges defining each span in the above ngrams construction. For instance, one may want to have the first token being `'Simple string` from range `[0,13[` with the empty space directly inside the string, and not given by the `subtoksep` attribute. \n",
    "\n",
    "This can be realized using the `ExtractionString.start` and `ExtractionString.stop` attributes, representing the first and last positions of the ranges of the `ExtractionString` _irrespective_ of the number of ranges. Said differently, one has to keep in mind there is no verification of the number of `intervals` element before calculating the `start` and `stop` attributes. For instance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0 13\n"
     ]
    }
   ],
   "source": [
    "print(ngrams_22[0].start, ngrams_22[0].stop)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and so to convert is quite easy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('Simple string', [(0,13)]),\n",
       " ExtractionString('string for', [(7,17)]),\n",
       " ExtractionString('for demonstration', [(14,31)]),\n",
       " ExtractionString('demonstration and', [(18,35)]),\n",
       " ExtractionString('and for', [(32,39)]),\n",
       " ExtractionString('for illustration', [(36,52)])]"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ngrams_23 = [ExtractionString(s.string, intervals=[s.start, s.stop]) \n",
    "             for s in ngrams_22]\n",
    "ngrams_23"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`ExtractionString.start` and `ExtractionString.stop` will play a prominent role when we will discuss the ordering relation among different instances of `ExtractionString` associated to the same parent string later in this introduction. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `ExtractionString.cuts`\n",
    "\n",
    "In order to help making n-grams and tokens, the method `split` has been designed. Let us try it "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString(' ', [(6,7)]),\n",
       " ExtractionString(' ', [(13,14)]),\n",
       " ExtractionString(' ', [(17,18)]),\n",
       " ExtractionString(' ', [(31,32)]),\n",
       " ExtractionString(' ', [(35,36)]),\n",
       " ExtractionString(' ', [(39,40)]),\n",
       " ExtractionString('.', [(52,53)])]"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "spans = span.split(cuts=intervals)\n",
    "spans"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One sees that `split` separates the initial span each time a range appear in the `cuts` parameter, _without detroying information_. This might not be the most suitable case for you, since one usually has to filter the irrelevant part of the splitting. Here the strategy could be to remove the empty `ExtractionString` (defined as having no child-string representation), the empty space spans and the non-alphanumerics span ... but it is far better to use the previous approach, and to feed a new `ExtractionString` instance directly with the outcome of the REGEX matches... \n",
    "\n",
    "In fact, here, and because we used REGEX to capture a pattern of contiguous alphanumeric characters as the relevant spans, one can simply capture half of the `ExtractionString`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('Simple', [(0,6)]),\n",
       " ExtractionString('string', [(7,13)]),\n",
       " ExtractionString('for', [(14,17)]),\n",
       " ExtractionString('demonstration', [(18,31)]),\n",
       " ExtractionString('and', [(32,35)]),\n",
       " ExtractionString('for', [(36,39)]),\n",
       " ExtractionString('illustration', [(40,52)])]"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "relevant_spans = span.copy()\n",
    "for s in spans:\n",
    "    relevant_spans.remove(s.intervals.start, \n",
    "                          s.intervals.stop)\n",
    "relevant_spans = relevant_spans.extractions\n",
    "relevant_spans"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One quite easily filters the different spans extracted at the previous step. An other usefull filter is the string comparison. For instance, to withdraw all the spans that contains the character `'o'` is intuitive"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('Simple', [(0,6)]),\n",
       " ExtractionString('string', [(7,13)]),\n",
       " ExtractionString('and', [(32,35)])]"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "relevant_spans_without_o = [s for s in relevant_spans if 'o' not in s]\n",
    "relevant_spans_without_o"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Char-grams and `ExtractionString.slice`\n",
    "\n",
    "The char-grams are even easier to implement, since the method `slice` does it for us, with parameters : \n",
    " \n",
    " - `start`: the character number where the slice starts\n",
    " - `stop`: the character number where the slice stops\n",
    " - `size`: the number of characters inside an outcoming `ExtractionString`\n",
    " - `step`: the number of characters skipped from one `ExtractionString` to the next one in the outcome\n",
    "\n",
    "The outcome is a list of `ExtractionString`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('Sim', [(0,3)]),\n",
       " ExtractionString('imp', [(1,4)]),\n",
       " ExtractionString('mpl', [(2,5)]),\n",
       " ExtractionString('ple', [(3,6)]),\n",
       " ExtractionString('le ', [(4,7)]),\n",
       " ExtractionString('e s', [(5,8)])]"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span.slice(start=0, stop=8, size=3, step=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that `.slice` applies straightforwardly even on already truncated string. Note the starting and stoping positions are calculated inside the child-string representation. For instance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[ExtractionString('strin', [(7,12)]),\n",
       " ExtractionString('tring', [(8,13)]),\n",
       " ExtractionString('ring ', [(9,13);(13,13)]),\n",
       " ExtractionString('ing f', [(10,13);(36,37)])]"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "span1.slice(size=5, stop=8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the `intervals` are cutted to give each child-string of the `ExtractionString` instance the correct length (this is the reason of the appearance of zero length `range(36,36)` for `ExtractionString('ring ', [(9,13),(36,36)])`, and this is also why one should not take a `subtoksep` of length higher than 1, because `ranges = [range(9,13), range(36,36)]` signifies that there are `13-9=4` characters plus one `subtoksep` character in this `ExtractionString`, while `range(36,36)` selects no string in the parent-string of this `ExtractionString` ; compare with the last `ExtractionString` of the above example, and change eventually the `subtoksep` to `'#'` character to see it in action). \n",
    "\n",
    "Note also that a `BoundaryWarning` easilly raises while using this method, since the default values are overwriten to start and stop on the size of the `str(ExtractionString)`. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Order relation among `ExtractionString`\n",
    "\n",
    "`ExtractionString` object can be ordered, providing that one keeps in mind \n",
    "\n",
    " - the order is only relevant for contiguous `ExtractionString`, a contiguous `ExtractionString` being a `ExtractionString` with a single interval, that is, this is a `ExtractionString` for which `start` and `stop` attribute make sense\n",
    " - the order is only relevant for two `ExtractionString` attached to the same string. This is somehow less sensible than the previous remark, since comparison of `ExtractionString`s would result in a `TypeError` in case their `ExtractionString.string` are not the same.\n",
    " \n",
    "Before entering into the details of the comparison, let us recall all the `ExtractionString` we constructed above\n",
    " \n",
    "```python\n",
    "'Simple string for demonstration and for illustration.' # the ExtractionString span\n",
    "'01234567891123456789212345678931234567894123456789512' # the positions\n",
    "\n",
    "'Simple string for demonstration and for illustration.' # the ExtractionString ngrams\n",
    "'012345 789112 456 8921234567893 234 678 412345678951 ' # the ranges\n",
    "```\n",
    "\n",
    "In particular, the span `ngrams` contains 7 `intervals`, and we can call sub-spans using the `subExtractionStrings` attribute. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `<` and `>` relations for non overlapping `ExtractionString`\n",
    "\n",
    "The `ExtractionString` class has been designed for latin languages (though it might work for any language that can be encoded in an alphabet), and the reading sense is _from left to right_.\n",
    "\n",
    " - `span1 < span2` means that the last position of `span1` is on the left of the first position of `span2` ; said differently `span1` is entirely read before `span2` is read\n",
    " - `span1 > span2` means that the first position of `span1` is on the right of the last position of `span2` ; said differently `span2` is entirely read before `span1` is read\n",
    "\n",
    "In particular, these definitions imply that : \n",
    " \n",
    " - if either `span1 < span2` or `span1 > span2`, they do not overlap\n",
    " - `span1 < span2` being false does not mean that `span1 > span2` : the two spans may overlap\n",
    " - `span1 > span2` being false does not mean that `span1 < span2` : the two spans may overlap\n",
    "\n",
    "Let us illustrate this : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "True\n",
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "# not overlapping ExtractionString\n",
    "print(ngrams.extractions[0] < ngrams.extractions[1])\n",
    "print(ngrams.extractions[1] > ngrams.extractions[0])\n",
    "print(ngrams.extractions[2] < ngrams.extractions[4])\n",
    "print(ngrams.extractions[4] > ngrams.extractions[2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To illustrate the strict order `<` and `>` on overlapping `ExtractionString`, let us cartoon the instances `span1` and `span2` from above\n",
    "\n",
    "```python\n",
    "'Simple string for demonstration and for illustration.' # the ExtractionString span\n",
    "'01234567891123456789212345678931234567894123456789512' # the positions\n",
    "\n",
    "'       string                       for illustration ' # the ExtractionString span1\n",
    "'       789112                       678 412345678951 ' # the ranges\n",
    "\n",
    "'       string for                       illustration ' # the ExtractionString span2\n",
    "'       789112 456                       412345678951 ' # the ranges\n",
    "```\n",
    "\n",
    "and recall the construction of `span1.start = 7`, `span1.stop = 51` and the same for `span2`: when it is about order comparison, a `ExtractionString` object associated to several `intervals` behaves as a monobloc entity from its starting to ending positions.\n",
    "\n",
    "Now the order of these two `ExtractionString` entities: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n",
      "False\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "# overlapping ExtractionString\n",
    "print(span1 < span2)\n",
    "print(span1 > span2)\n",
    "print(span2 < span1)\n",
    "print(span2 > span1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `<=` and `>=` relations\n",
    "\n",
    "For overlapping spans, the orders of the `ExtractionString` can either be\n",
    "\n",
    " - `span1 <= span2` if there are possibly non-overlapping part of `span1` on the left of `span2` ; said differently the left-most part of the union of `span1` and `span2` belongs to `span1` ; said differently `span1` is partly read before `span2` is read\n",
    " - `span1 >= span2` if there are possibly non-overlapping part of `span1` on the right of `span2` ; said differently the right-most part of the union of `span1` and `span2` belongs to `span1` ; said differently `span2` is partly read before `span1` is read\n",
    "\n",
    "From the above definition, note that\n",
    "\n",
    " - if both `span1 >= span2` and `span1 <= span2` were true, there is no reason why `span1 == span2` should be true. In fact we defined the overlapping orders `<=` and `>=` to be independent to each other: there is no way that `span1 >= span2 and span1 <= span2` returns `True`\n",
    "\n",
    "There is no difficulty in interpretation, as illustrated with `span1` and the `ExtractionString` including the string `'documentation'` from `ngrams` : `documentation` is on the right of `span1` and so both `documentation` is on the right of `span1` (`documentation >= span1`) and `span1` is on the left of `demonstration` (`span1 <= demonstration`). To insist one more time : `span1` starts before `demonstration`, and there are overlapping part between the two `ExtractionString` (this is counter-intuitive if one forgets the roles of `span1.start` and `span1.stop` in the ordering relations). Those are the only true orders in presence."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n",
      "False\n",
      "False\n",
      "False\n",
      "True\n",
      "True\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "# overlapping span with no common boundary\n",
    "demonstration = ngrams.extractions[3]\n",
    "print(demonstration < span1)\n",
    "print(demonstration > span1)\n",
    "print(span1 < demonstration)\n",
    "print(span1 > demonstration)\n",
    "print(demonstration <= span1)\n",
    "print(demonstration >= span1)\n",
    "print(span1 <= demonstration)\n",
    "print(span1 >= demonstration)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same easiness of interpretation is present in the case of one boundary in common, as e.g."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n",
      "True\n",
      "False\n",
      "False\n",
      "False\n",
      "False\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "# overlapping span with left common boundary\n",
    "string = ngrams.extractions[1]\n",
    "print(string < span1)\n",
    "print(string > span1)\n",
    "print(string <= span1)\n",
    "print(string >= span1)\n",
    "print(span1 < string)\n",
    "print(span1 > string)\n",
    "print(span1 <= string)\n",
    "print(span1 >= string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "for left boundary in common, and "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n",
      "False\n",
      "True\n",
      "False\n",
      "False\n",
      "True\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "# overlapping span with right common boundary\n",
    "illustration = ngrams.extractions[-1]\n",
    "print(illustration < span1)\n",
    "print(illustration > span1)\n",
    "print(illustration <= span1)\n",
    "print(illustration >= span1)\n",
    "print(span1 < illustration)\n",
    "print(span1 > illustration)\n",
    "print(span1 <= illustration)\n",
    "print(span1 >= illustration)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "for right boundary in commom. In contrary, if the two boundaries are in common, there is no order relation between the different `ExtractionString`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n",
      "False\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "# overlapping ExtractionString with two common boundaries\n",
    "print(span1 <= span2)\n",
    "print(span1 >= span2)\n",
    "print(span2 <= span1)\n",
    "print(span2 >= span1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Complete order of `ExtractionString` on a parent string\n",
    "\n",
    "The interest in this order is that, whatever the two choosen `ExtractionString`s associated with a common string, there is one and only one valid assumption, either: \n",
    "\n",
    " - `span1 > span2`: the two spans do not overlap, and `span1` is entirely on the right of `span2`,\n",
    " - `span1 < span2`: no overlap, `span1` is entirely on the left of `span2`, \n",
    " - `span1 >= span2`: overlapping string, with `span1` finishing later than `span2` in the reading order,\n",
    " - `span1 <= span2`: overlapping string, with `span2` finishing later than `span1` in the reading order, \n",
    "\n",
    "and the order is dependent of the order of the `ExtractionString`s, that is, we should not try to interpret the outcome of `span1 <= span2 and span2 <= span1`, that just signifies that `span1.start == span2.start`. \n",
    "\n",
    "Recall that the order comparison takes into account only the complete size of the `ExtractionString`, that is, it compares only the `start` and `stop` attributes of the `ExtractionString`, disregarding the possibility of non-contiguous `ExtractionString`. Said differently, `span1` above is treated (in terms of comparisons) in the same way as `span2` (and this is also why one has both `span1 <= span2` and `span2 <= span1` in order to preserve the unique order relations)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algebraic manipulations of `ExtractionString`\n",
    "\n",
    "Since the `ExtractionString` object represents a collection of characters positions from a parent string, and because a position cannot appear more than once (recall overlapping intervals in `ExtractionString.ranges` are forbidden), one can treat this collection as a mathematical set. So the basic set operations of union, difference, intersection, and symmetric division are allowed. \n",
    "\n",
    "Here are their results, illustrated from `span1` and `span2`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Union\n",
    "\n",
    "The complete set of all positions in `span1` plus all positions in `span2`. This operation is symmetric."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('string for for illustration', [(7,13);(14,17);(36,39);(40,52)])"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# union\n",
    "span1 + span2\n",
    "# equals to span1.union(span2), span2+span1, span2.union(span1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Difference\n",
    "\n",
    "The portion of the parent string in `span1` that is absent in `span2` is represented as `span1.difference(span2)`. Note this is not a symmetric operation (warn the `ExtractionString('for')` with different `intervals`...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('for', [(36,39)])"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# difference\n",
    "span1 - span2\n",
    "# equals to span1.difference(span2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('for', [(14,17)])"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# difference is not symmetric\n",
    "span2.difference(span1)\n",
    "# equals to span2 - span1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Intersection\n",
    "\n",
    "The `intervals` which are common to both `span1` and `span2` can be found using `span1 * span2` or `span1.intersection(span2)`. This operation is symmetric `span1 * span2 == span2 * span1` is `True`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('string illustration', [(7,13);(40,52)])"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# intersection\n",
    "span1 * span2\n",
    "# equals to span1.intersection(span2), span2*span1, span2.intersection(span1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Symmetric difference\n",
    "\n",
    "The symmetric difference represent the union minus the intersection of the two `ExtractionString`. It is repsented by the division operator `/`, but keep in mind this relation is symmetric."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('for for', [(14,17);(36,39)])"
      ]
     },
     "execution_count": 36,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# symmetric_difference\n",
    "span1/span2\n",
    "# equals to span1.symmetric_difference(span2), span2/span1, ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "The `ExtractionString` object is a convenient abstraction of the processus of cutting a string into sub-parts. Instead of constructing all string possibilities before feeding them to the next stage of the NLP treatment, one can apply some construction steps on their characters positions, which in principle is far more efficient in terms of algorithms. In addition to reconstruct the usual tokens (e.g. n-grams and char-grams for instance), the main advantages of the `ExtractionString` class is its habilities to construct as many children string as one wants, starting from a unique parent string. Using the four order relations `<`, `>`, `<=` and `>=` among all these children, as well as the infinite possibilities of combining these children strings using the `+`, `-`, `*` and `/` operations allow overwhelming new routes of manipulating a string in an abstract way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Last modification Tue Jan  3 15:13:58 2023\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "print(\"Last modification {}\".format(datetime.now().strftime(\"%c\")))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
