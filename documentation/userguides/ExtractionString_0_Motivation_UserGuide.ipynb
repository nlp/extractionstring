{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ExtractionString class -  Motivations\n",
    "\n",
    "We introduce the `ExtractionString` class, as a usefull tool for later implementations of Natural Language Processing (NLP) tasks in a versatile, yet efficient and easy to run environment.\n",
    "\n",
    "## Motivation\n",
    "\n",
    "At its most basic level, the only manipulations one can apply to a string are : \n",
    "\n",
    " - extraction of a part of a parent string into a child string, this is the **extraction** process\n",
    " - insertion or deletion of a child string from a parent string, this is the **substitution** process\n",
    "\n",
    "We here focus on the extraction (sometimes called tokenization, see below) process, the substitution process being discussed in an [other module of ours](https://framagit.org/nlp/substitutionstring).\n",
    "\n",
    "### Extraction is a selection of intervals from the parent string\n",
    "\n",
    "If one describes any sub-string of a parent string by the range (or interval) of its positions of characters, then extracting one sub-string from this parent string can be seen as selecting an interval of positions inside the complete range of available positions in the parent string. For instance, the string `'Simple string for demonstration and for illustration.'` has length 52 and the interval `[7,13[` corresponds to the sub-string `'string'`, as illustrated in the following cartoon\n",
    "\n",
    "```python\n",
    "'Simple string for demonstration and for illustration.' # the parent string\n",
    "'01234567891123456789212345678931234567894123456789512' # the positions\n",
    "\n",
    "'       string                       for illustration ' # the ExtractionString extracst1\n",
    "'       789112                       678 412345678951 ' # the corresponding intervals\n",
    "\n",
    "'       string for                       illustration ' # the ExtractionString extracst2\n",
    "'       789112 456                       412345678951 ' # the corresponding intervals\n",
    "```\n",
    "\n",
    "In addition, there is nothing which forbids to take several intervals at once, as for instance the sub-string `'string for illustration'` that corresponds to the _union_ of intervals `[7,13[`, `[36,39[` and `[40,52[` in the above example.\n",
    "\n",
    "Thus in practice, to cut a string consists in associating to a given object a parent string and a collection of ranges. This is what we model as an `ExtractionString`, described by \n",
    "\n",
    " - a parent string: this is the attribute `ExtractionString.string`\n",
    " - a collection of intervals: this is the attribute `ExtractionString.intervals` that corresponds to a list of integers\n",
    "\n",
    "(in fact there is one last attribute: `ExtractionString.subtoksep`, that allows to re-glue the sub-intervals into a consistent new string)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Differences between extraction and tokenization\n",
    "\n",
    "The motivation of the present library is to represent the tokenization process as the _extraction of characters positions that corresponds to sub-string_ instead of the _extraction of sub-string directly_ as it is usually done in other libraries dealing with Natural Language Processing (NLP). This alternative approach allows much more \n",
    "\n",
    " - flexibility, since one can associate an infinite collection of tokens (called `ExtractionString` in this library) to a given parent string, and algebraically manipulate them by union, difference, intersection, ... of their positions sets\n",
    " - rigor, since e.g. the `span1` and `span2`, having the same string representation `'string for illustration'`, have no more reason to be equal because their position sets are not the same ; instead there is an order relation between them, since they have a non-empty intersecting sub-set in common (namely `'string illustration'`)\n",
    " \n",
    "In addition, one can easilly reconstruct the basic tokenizers (for instance n-grams or char-grams) using the `ExtractionString` class, as we will illustrate in a moment. \n",
    "\n",
    "The only restriction one has to impose to the `ExtractionString` concept is to forbids overlapping ranges of position. Practically, if an overlap appears, the `ExtractionString` class will construct the union of the underneath ranges. This is not a drastic restriction, since one can always construct as many `ExtractionString` objects as one wants, and attach them to the same parent string in order to compare them.\n",
    "\n",
    "As string objects can not be fed to a computer in order to involve mathematical manipulation (especially prediction), one basic method to treat a text is to separate it into atomic string entities, called _tokens_, and vectorize them as a first step of any subsequent mathematical analysis of documents. Those tokens can be just\n",
    " - words : say, string entities separated by spaces,\n",
    " - bi-grams : mobile windows of two consecutive words along the document,\n",
    " - n-grams : mobile windows of n consecutive words,\n",
    " - sentence : say, string entities separated par backspace `\\n`,\n",
    " - n-chargrams : mobile windows of n consecutive characters,\n",
    "or anything in between, that is, complicated association of characters, words, hyphenation, sentences, ellipsis, ...\n",
    "\n",
    "In order to manipulate those elements from a computer, one needs a versatile object able to handle many different situations in a unified manner. Unfortunately, as actually implemented in the standard libraries, tokenization suffers from some limitations. For the sake of convenience, we focus only on some popular `Python` implementations of tokenization, in a separate document available on the [official documentation](../tutorials/comparison_other_libraries) ; below we suppose the reader is aware of the limitations of the most current Python libraries for NLP.\n",
    "\n",
    "In order to avoid hindering tokenization specificities inside an end-to-end algorithm having tokenization at its basis (as for `sklearn` or `spaCy` libraries), we want to create a kind of tokenization class that will do only tokenization process. In order to avoid getting simple strings as tokenizer output (as for `nltk` library), we will create an object which can be easily adapted to later usages at will."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tokenization as mapping to a lexic of lexemes\n",
    "\n",
    "An other interpretation of the tokenization process consists in the mapping of the parent string into a collection of coherent strings taken in a lexic. In this approach, the set of coherent strings may not be exact extraction of the parent string: for instance the tokenizer may correct some misspellings in the way to the text atomisation. The lemmatisation proces is one similar example of such a tokenization. The tokenization process consists in producing a unique representation of atomic entities from the parent string. This is the usual way linguist try to cut a text into non-eparable quantities (what we call string-atomisation here). Computer scientist usually do not usually think the preprocessing of a text that way. Here, we do not refer to any lexic of lexemes, and we prefer to call the procedure of splitting a string in sub-parts as extracting-string, hence the name of the library. There is no notion of unicicity of the atomisation in our protocol."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "remove-input"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Last modification Tue Jan  3 14:45:29 2023\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "print(\"Last modification {}\".format(datetime.now().strftime(\"%c\")))"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
