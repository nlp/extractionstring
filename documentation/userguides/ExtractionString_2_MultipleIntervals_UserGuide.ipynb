{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# `ExtractionString` class -  Chapter 2 : multiples intervals\n",
    "\n",
    "In the previous chapter, we introduced the `ExtractionString` object as simple representation of a Python string, with additional basic methods allowing to split and partition the string, in addition to the algebraic and ordering properties of this simple object. \n",
    "\n",
    "There are several remarks in this NoteBook about the construction and limits of the design. They can be droped at first reading. In fact, most of the `intervals` and `subtoksep` properties are of no interest for basic usages of the `ExtractionString` class, so feel free to pass most of the materials covered in the present note and pass directly to the [tutorial example](../tutorials/lucene_tokenization), where one implements a simple tokenizer in details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary of the `intervals` and `subtoksep` attributes\n",
    "\n",
    "Every `ExtractionString` object has the following attributes : \n",
    " - `string` : the associated complete parent string.\n",
    " - `intervals` : the intervals into the `string` which defines the string representation of the `ExtractionString`, namely `str(ExtractionString)`. This attribute is a list of integers of even size (in practice it is an [`EvenSizedSortedSet` object, described in an other user guide](EvenSizedSortedSet_UserGuide).\n",
    " - `subtoksep` : when there are several intervals in the `intervals` attribute, the string representation `str(ExtractionString)` glues the different sub-tokens with `subtoksep` as the separator. To avoid missing some usefull behaviors, one advises to use a one-length string element as `subtoksep`, default being the space symbol `chr(32)` in Python terminology.\n",
    "\n",
    "We start by instanciating a simple string, wich will serve as support for later illustrations of the `intervals` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from extractionstring import ExtractionString, EvenSizedSortedSet\n",
    "text = \"A really simple string for illustration.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic Usage of `intervals` attribute\n",
    "\n",
    "Basic usage of the `ExtractionString` class is as a container for a string. It is constructed from a string, with argument `string` at the instanciation. More precisely, it has a sub-string behavior from the complete `ExtractionString.string` string. The way one pass from the complete string to its sub-string representation is through the `intervals` attribute. This attribute can be designed by hand, as we will do in the following for illustration. \n",
    "\n",
    "Recall that without `intervals` parameter when one instanciates the `ExtractionString` object, the `ExtractionString.intervals` attribute is designed to describe the entire `ExtractionString.string` string. \n",
    "\n",
    "Recall also that the `intervals` attribute must be a list of integers coming in pairs, whatever might be the length of this list. In particular, for a single pair `start, stop`, `ExtractionString.intervals=[start,stop]` is the standard convention. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "EvenSizedSortedSet[(0,40)]\n",
      "A really simple string for illustration.\n",
      "A really simple string for illustration.\n",
      "########################################\n",
      "EvenSizedSortedSet[(0,8)]\n",
      "A really\n",
      "A really simple string for illustration.\n",
      "########################################\n",
      "EvenSizedSortedSet[(9,22)]\n",
      "simple string\n",
      "A really simple string for illustration.\n",
      "########################################\n"
     ]
    }
   ],
   "source": [
    "extracst = ExtractionString(string=text)\n",
    "print(extracst.intervals)\n",
    "print(str(extracst))\n",
    "print(extracst.string)\n",
    "print(\"#\"*len(extracst.string))\n",
    "\n",
    "extracst = ExtractionString(string=text, intervals=[0,8])\n",
    "print(extracst.intervals)\n",
    "print(str(extracst))\n",
    "print(extracst.string)\n",
    "print(\"#\"*len(extracst.string))\n",
    "\n",
    "extracst = ExtractionString(string=text, intervals=[9,22])\n",
    "print(extracst.intervals)\n",
    "print(str(extracst))\n",
    "print(extracst.string)\n",
    "print(\"#\"*len(extracst.string))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us construct a few `ExtractionString` from the same string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A really\n",
      "simple string\n",
      "illustration\n",
      "A simple string illustration\n"
     ]
    }
   ],
   "source": [
    "extracst1 = ExtractionString(string=text,intervals=[0,8])\n",
    "print(str(extracst1))\n",
    "extracst2 = ExtractionString(string=text,intervals=(9,22))\n",
    "print(str(extracst2))\n",
    "extracst3 = ExtractionString(string=text,intervals=(27,39))\n",
    "print(str(extracst3))\n",
    "extracst4 = ExtractionString(string=text,intervals=(0,1, 9,22, 27,39))\n",
    "print(str(extracst4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One sees that there is not much differences between a string representation of a `ExtractionString` object having one or several intervals. This is in fact where the `ExtractionString.subtoksep` appears. Let us change this parameter for the above examples. By default this parameter is the space symbol."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A really\n",
      "simple string\n",
      "illustration\n",
      "A__sep__simple string__sep__illustration\n"
     ]
    }
   ],
   "source": [
    "extracst1_sep = ExtractionString(string=text,\n",
    "             intervals=[0, 8],\n",
    "             subtoksep='__sep__')\n",
    "print(str(extracst1_sep))\n",
    "extracst2_sep = ExtractionString(string=text,\n",
    "             intervals=[9,22],\n",
    "             subtoksep='__sep__')\n",
    "print(str(extracst2_sep))\n",
    "extracst3_sep = ExtractionString(string=text,\n",
    "             intervals=[27,39],\n",
    "             subtoksep='__sep__')\n",
    "print(str(extracst3_sep))\n",
    "extracst4_sep = ExtractionString(string=text,\n",
    "             intervals=[0,1, 9,22, 27,39],\n",
    "             subtoksep='__sep__')\n",
    "print(str(extracst4_sep))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While there is no change when there is a single interval in the `ExtractionString`, the string rendering changes when there are several intervals in the object. In that later case (see `extracst4` above), calling `str(ExtractionString)` automatically glues the different sub-tokens using the `subtoksep` attribute. The usual spaces (as any other character in fact) inside a given interval is not affected by the `subtoksep`, see `'simple string'`-substring in `extracst4`.\n",
    "\n",
    "## Slicing procedure in a `ExtractionString`\n",
    "\n",
    "How is the slicing process handled in a `ExtractionString`? Well, the `subtoksep` counts as any other character in the string representation, and its string representation (particularly, its length) counts as well. It also counts in `len` in fact. Let us illustrate this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A rea\n",
      "A sim\n",
      "ple s\n",
      "tring illu\n"
     ]
    }
   ],
   "source": [
    "print(extracst1[:5])\n",
    "print(extracst4[:5])\n",
    "print(extracst4[5:10])\n",
    "print(extracst4[10:20])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "28\n",
      "5\n"
     ]
    }
   ],
   "source": [
    "print(len(extracst4))\n",
    "print(len(extracst4[:5]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And if we change the `subtoksep`, its length is automatically taken into account for the calculation of the length of the complete `ExtractionString` and the slicing process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A_&_simple string_&_illustration\n",
      "32\n",
      "A_&_s\n"
     ]
    }
   ],
   "source": [
    "extracst5 = ExtractionString(\n",
    "    string=text,\n",
    "    intervals=[0,1, 9,22, 27,39],\n",
    "    subtoksep='_&_')\n",
    "print(str(extracst5))\n",
    "print(len(extracst5))\n",
    "print(extracst5[:5])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the `ExtractionString` class handles all the machinery of a quite normal string, thanks to the `intervals` and `subtoksep` attributes.\n",
    "\n",
    "## Absolute and relative coordinates\n",
    "\n",
    "An `ExtractionString` object may look like just a sub-string of its `string` attribute. There are nevertheless some subtleties underneath. One of them is the absolute versus relative coordinates, which may give headaches to users. Fortunately enough, things are quite workable, trusting the machinery behind the `ExtractionString` class. \n",
    "\n",
    "The _absolute coordinate_ system is the position of the text inside the parent string given at the instanciation of the `ExtractionString` object. Most of the time, one should not worry about it, except perhaps in the `append(start, stop)` and `remove(start, stop)` methods that will be discussed later in this chapter.\n",
    "\n",
    "The _relative coordinate_ system is the position inside the `intervals` sub-object. This is the natural position if one sees the `ExtractionString` as its string representation `str(ExtractionString)`.\n",
    "\n",
    "So in short : \n",
    " - absolute position refers to the position in the string `ExtractionString.string`\n",
    " - relative coordinate refers to the position in the string `str(ExtractionString)`\n",
    "\n",
    "As an example, let us construct a simple string of digits, where the digit `0` appears at position `0`, the digit `1` at position `10`, the digit `2` at position `20` and so on. In between two decades, there are the natural digits from `1` to `9`. On top of this string, we construct the `ExtractionString` which will represent a string of size `40` made of one decade over two, up to position `80` of the `digits` string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'1123456789#3123456789#5123456789#7123456789'"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "root = '123456789'\n",
    "digits = ''.join([str(i) + root for i in range(10)])\n",
    "extracst_digits = ExtractionString(\n",
    "    string=digits,\n",
    "    intervals=[10,20, 30,40, 50,60, 70,80],\n",
    "    subtoksep='#')\n",
    "str(extracst_digits)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then the relative coordinates range from `0` to `39+3*len(subtoksep)` since there are 4 ranges and so 3 `subtoksep` separators inserted, whereas the absolute ones range from `0` to `99` without interruption of a `subtoksep`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Relative coordinates from 0 to 20\n",
      "1123456789#312345678\n",
      "Relative length: 43\n",
      "\n",
      "\n",
      "Absolute coordinates from 0 to 20\n",
      "01234567891123456789\n",
      "Absolute length: 100\n"
     ]
    }
   ],
   "source": [
    "print(\"Relative coordinates from 0 to 20\")\n",
    "print(str(extracst_digits)[:20])\n",
    "print(\"Relative length: {}\".format(len(extracst_digits)))\n",
    "print(\"\\n\")\n",
    "print(\"Absolute coordinates from 0 to 20\")\n",
    "print(extracst_digits.string[:20])\n",
    "print(\"Absolute length: {}\".format(len(extracst_digits.string)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One more time, this is just a quite ridiculous complexity in the presentation for almost nothing, since most of the usages will never find un-natural outcome using the basic tools of `ExtractionString` and `ExtractionStrings` classes, as long as `subtoksep` is of length 1. \n",
    "\n",
    "## Enlarge the ExtractionString, and combine overlapping ranges\n",
    "\n",
    "Suppose one want to collapse, for some reason, `extracst1` and `extracst2` in a new `ExtractionString` called `extracst12`. Then the related string representation will be given by the concatenation of the two previous strings, and the resulting `intervals` attribute illustrate the combination, as well as the `subtoksep` that is now present."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A really simple string\n",
      "EvenSizedSortedSet[(0,8);(9,22)]\n"
     ]
    }
   ],
   "source": [
    "extracst12 = extracst1 + extracst2\n",
    "print(extracst12)  # equivalent to print(str(extracst12))\n",
    "print(extracst12.intervals)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this `ExtractionString` has not mush difference with the initial string from position 0 to 22, except for the `subtoksep` that is different from a normal space symbol in our illustration: it is inherited from `extracst1` by construction of `extracst1 + extracst2`.\n",
    "\n",
    "```admonition\n",
    ":class: note\n",
    "Recall `__add__` is similar to `union` method, such that `extracst1 + extracst2 == extracst1.union(extracst2)` returns `True`. Also, `__sub__`, `__mul__` and `__truediv__` methods are aliases for `difference`, `intersection` and `symmetric_difference`.\n",
    "```\n",
    "\n",
    "```admonition\n",
    ":class: danger\n",
    "Note in passing that there are blocking process that avoid adding two `ExtractionString` if they do not have the same `subtoksep`...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can also combine contiguous (= single interval) `ExtractionString` with dis-contiguous (= multiple intervals) objects as well"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ExtractionString('A really simple string illustration', [(0,8);(9,22);(27,39)])"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "extracst1 + extracst5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us come back to the concatenation procedure. What would happen if one tries to concatenate `extracst3` and `extracst4`, since they have a part of the initial string in common ? In fact there are special handling underneath, which will recalculate all `intervals` such that overlapping disapears."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A simple string illustration\n",
      "EvenSizedSortedSet[(0,1);(9,22);(27,39)]\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "extracst34 = extracst3 + extracst4\n",
    "print(extracst34)\n",
    "print(extracst34.intervals)\n",
    "print(extracst34 == extracst4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```admonition\n",
    ":class: note\n",
    "We will come back to the comparison of `ExtractionString`s later.\n",
    "```\n",
    "\n",
    "Obviously, the addition of `ExtractionString` is commutative."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A simple string illustration\n",
      "EvenSizedSortedSet[(0,1);(9,22);(27,39)]\n",
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "extracst43 = extracst4 + extracst3\n",
    "print(extracst43)\n",
    "print(extracst43.intervals)\n",
    "print(extracst43 == extracst4)\n",
    "print(extracst34 == extracst43)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To illustrate further the non-overlapping catching, let us try to create a new `ExtractionString` with some overlapping intervals, and realise that the construction in fact destroys the independant `intervals` and fuse them towards a single-`intervals` `ExtractionString` instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A really simple string_illustration\n",
      "EvenSizedSortedSet[(0,22);(27,39)]\n"
     ]
    }
   ],
   "source": [
    "intervals = EvenSizedSortedSet(0,9)\n",
    "intervals.append(9,22).append(27,39).append(30,39).append(10,15)\n",
    "\n",
    "extracst6 = ExtractionString(\n",
    "    string=text,\n",
    "    intervals=intervals,\n",
    "    subtoksep='_')\n",
    "print(extracst6)\n",
    "print(extracst6.intervals)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are only two `intervals` surviving the construction process, since interval `[10,15[` is entirely contained in `[9,22[`, and the same is true for `[30,39[` which gives no more information than `[27,39[` relative to the initial string. In addition the two intervals `[0,9[` and `[9,22[` are transformed naturally to the `[0,22[` interval since they represent this entire range when taken together."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `ExtractionString.append` and `ExtractionString.remove`\n",
    "\n",
    "There are two facilities to design the `ExtractionString.intervals` attributes, namely \n",
    "\n",
    " - adding an interval `[start,stop[` to `ExtractionString.intervals` using the method `append(start, stop)`, \n",
    " - removing an interval `[start,stop[` using `remove(start, stop)`. \n",
    "\n",
    "```admonition\n",
    ":class: danger\n",
    "Note that appending an interval using `ExtractionString.append` also checks for overlapping interval, and will not duplicate the intervals in `ExtractionString.intervals`. Since one wants to append a new intervals, this intervals is given in absolute coordinates, that is, in the counting of `ExtractionString.string`.\n",
    "\n",
    "In the contrary, `ExtractionString.remove` will withdraw the passing interval from the _relative_ coordinates. That is, if the removed interval is not overlapping with some `ExtractionString.intervals`, it will not be removed. Nevertheless, `ExtractionString.remove` uses the absolute coordinates.\n",
    "\n",
    "Importantly, `append`, `remove` and their aliases work _in place_, i.e. they transform the `ExtractionString` object itself. This is the same behavior as the same methods of a Python list.\n",
    "```\n",
    "\n",
    "To illustrate this, we come back to our digits string introduced in [the discussion about absolute and relative coordinates](#Absolute-and-relative-coordinates) above. The `_digits` instance has `ranges` attributes in the form `[(10,20),(30,40),(50,60),(70,80)]`, so appending the range `(20,30)` should fuse its two first sub-ranges because the overlapping are automatically fusionned after an `ExtractionString.append` process. From the resulting `ExtractionString.append` one can remove the same range `(20,30)` to come back to the intial object. If one remove the range `(15,35)`, then one should end up with a `ExtractionString.ranges` of the form `[(10,25),(35,40),(50,60),(70,80)]`. Let us see all of this (plus a few more) in the examples below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "EvenSizedSortedSet[(10,20);(30,40);(50,60);(70,80)]\n",
      "append (20,30)\n",
      "EvenSizedSortedSet[(10,40);(50,60);(70,80)]\n",
      "remove (20,30)\n",
      "EvenSizedSortedSet[(10,20);(30,40);(50,60);(70,80)]\n",
      "remove (15,35)\n",
      "EvenSizedSortedSet[(10,15);(35,40);(50,60);(70,80)]\n",
      "remove (15,35) and (75,125), far too long for the ExtractionString.string\n",
      "EvenSizedSortedSet[(10,15);(35,40);(50,60);(70,75)]\n"
     ]
    }
   ],
   "source": [
    "print(extracst_digits.intervals)\n",
    "print(\"append (20,30)\")\n",
    "extracst_digits.append(20,30)\n",
    "print(extracst_digits.intervals)\n",
    "print(\"remove (20,30)\")\n",
    "extracst_digits.remove(20,30)\n",
    "print(extracst_digits.intervals)\n",
    "print(\"remove (15,35)\")\n",
    "extracst_digits.remove(15,35)\n",
    "print(extracst_digits.intervals)\n",
    "print(\"remove (15,35) and (75,125), far too long for the ExtractionString.string\")\n",
    "extracst_digits.remove_intervals(15,35, 75,125)\n",
    "print(extracst_digits.intervals)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One sees that removing an interval that does not exist in the absolute coordinates produce nothing (that's the example of removing the interval `[75,125[` at the last step, which in fact remove only the interval `[75,80[` as this is the only available one in the `ExtractionString` object at that step).\n",
    "\n",
    "In the same way, adding an interval from the outside of the `ExtractionString.string` will produce nothing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "EvenSizedSortedSet[(10,15);(35,40);(50,60);(70,75)]\n",
      "append (120,130)\n",
      "EvenSizedSortedSet[(10,15);(35,40);(50,60);(70,75);(120,130)]\n"
     ]
    }
   ],
   "source": [
    "print(extracst_digits.intervals)\n",
    "print(\"append (120,130)\")\n",
    "extracst_digits.append(120,130)\n",
    "print(extracst_digits.intervals)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we pass to the basic explanation of how the `ExtractionString.intervals` transform when invoking `partition`, `split` or `slice` methods.\n",
    "\n",
    "## Construct sub-`ExtractionString` and tokens\n",
    "\n",
    "The three mechanisms to pass from `ExtractionString` to `ExtractionStrings` are using either the `partition`, `split` or `slice` methods. We review they mechanisms once a multi-ranged `ExtractionString` is involved in the process. \n",
    "\n",
    "One more time, we prefer the debugging representation of the `ExtractionStrings` class for illustration, or its list representation, than the `str(ExtractionStrings)` representation, considered more messy.\n",
    "\n",
    "We first see that everything is done to not bother the user with the position arguments, the `start` and `stop` parameter of `ExtractionString.partition` is calculated from the string representation `str(ExtractionString)` (relative coordinates). In addition, the `subtoksep` are conserved by the splitting processes. So, when one cuts the `extracst4` string from position `start=2` to position `stop=8` using the `partition` method, one really isolates `str(extracst4)[2:8]`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ExtractionString('A ', [(0,1);(1,1)]), ExtractionString('simple', [(9,15)]), ExtractionString(' string illustration', [(15,22);(27,39)])]\n",
      "True\n",
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "#extracst4.subtoksep=chr(32)\n",
    "extracsts = extracst4.partition(2,8)\n",
    "print(list(extracsts))\n",
    "print(str(extracsts[0])==str(extracst4)[:2])\n",
    "print(str(extracsts[1])==str(extracst4)[2:8])\n",
    "print(str(extracsts[2])==str(extracst4)[8:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The way the `subtoksep` are conserved is due to the insertion of empty `intervals` in the `ExtractionString` combined: see the first line below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "EvenSizedSortedSet[(0,1);(1,1)]\n",
      "EvenSizedSortedSet[(9,15)]\n",
      "EvenSizedSortedSet[(15,22);(27,39)]\n"
     ]
    }
   ],
   "source": [
    "for tok in extracsts:\n",
    "    print(tok.intervals)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note finally that the behavior is less clear as soon as one use a `subtoksep` with length larger than 1, as is illustrated below, where several different solutions exist for the same strings. This is because it is quite clear in Python what to do with a `range(start,stop)` which always corresponds to a semi-open (mathematical) interval `[start,stop[` including the `start` and rejecting the `stop`, but when either the `start` or the `stop` pops on a `subtoksep`, does one have to put it in the left or in the right interval after splitting the string ? The answer is below : if you design the splitting to be performed for a one-character sized `subtoksep`, the algorithm wait for the `subtoksep` to be entirely on the left of `stop` to display it in the left `range`. See the illustration below.\n",
    "\n",
    "**This is the reason why using `len(subtoksep)==1` is highly recommended.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ExtractionString('A', [(0,1)]), ExtractionString('_&__&__&_simp', [(9,9);(9,9);(9,9);(9,13)]), ExtractionString('le string_&_illustration', [(13,22);(27,39)])]\n",
      "total length = 38\n",
      "[ExtractionString('A_&_', [(0,1);(1,1)]), ExtractionString('_&__&_simp', [(9,9);(9,9);(9,13)]), ExtractionString('le string_&_illustration', [(13,22);(27,39)])]\n",
      "total length = 38\n",
      "[ExtractionString('A_&__&_', [(0,1);(1,1);(1,1)]), ExtractionString('_&_simpl', [(9,9);(9,14)]), ExtractionString('e string_&_illustration', [(14,22);(27,39)])]\n",
      "total length = 38\n",
      "[ExtractionString('A_&__&__&_', [(0,1);(1,1);(1,1);(1,1)]), ExtractionString('simple', [(9,15)]), ExtractionString(' string_&_illustration', [(15,22);(27,39)])]\n",
      "total length = 38\n"
     ]
    }
   ],
   "source": [
    "extracsts = extracst5.partition(1,8)\n",
    "print(list(extracsts))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in extracsts)))\n",
    "extracsts = extracst5.partition(2,8)\n",
    "print(list(extracsts))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in extracsts)))\n",
    "extracsts = extracst5.partition(3,9)\n",
    "print(list(extracsts))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in extracsts)))\n",
    "extracsts = extracst5.partition(4,10)\n",
    "print(list(extracsts))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in extracsts)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In contrary, using a correct `subtoksep` with length 1 never destroys the `ExtractionString` string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ExtractionString('A', [(0,1)]), ExtractionString(' simple', [(9,9);(9,15)]), ExtractionString(' string illustration', [(15,22);(27,39)])]\n",
      "total length = 28\n",
      "[ExtractionString('A ', [(0,1);(1,1)]), ExtractionString('simple', [(9,15)]), ExtractionString(' string illustration', [(15,22);(27,39)])]\n",
      "total length = 28\n"
     ]
    }
   ],
   "source": [
    "extracsts = extracst4.partition(1,8)\n",
    "print(list(extracsts))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in extracsts)))\n",
    "extracsts = extracst4.partition(2,8)\n",
    "print(list(extracsts))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in extracsts)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An other important special case is when one tries to split the initial `ExtractionString` on either its initial or final character. See the example below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ExtractionString('', []), ExtractionString('A', [(0,1)]), ExtractionString(' simple string illustration', [(9,9);(9,22);(27,39)])]\n",
      "[ExtractionString('A simple string ', [(0,1);(9,22);(22,22)]), ExtractionString('illustration', [(27,39)]), ExtractionString('', [])]\n"
     ]
    }
   ],
   "source": [
    "extracsts = extracst4.partition(0,1)\n",
    "print(list(extracsts))\n",
    "extracsts = extracst4.partition(len(extracst4)-12,len(extracst4))\n",
    "print(list(extracsts))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In that case, the left-most of right-most `ExtractionString` is empty. Nevertheless, one can remedy to that by using the parameter `remove_empty=True` (default is `False`) when calling `partition`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ExtractionString('A', [(0,1)]), ExtractionString(' simple string illustration', [(9,9);(9,22);(27,39)])]\n",
      "[ExtractionString('A simple string ', [(0,1);(9,22);(22,22)]), ExtractionString('illustration', [(27,39)])]\n"
     ]
    }
   ],
   "source": [
    "extracsts = extracst4.partition(0,1,remove_empty=True)\n",
    "print(list(extracsts))\n",
    "extracsts = extracst4.partition(len(extracst4)-12,len(extracst4),True)\n",
    "print(list(extracsts))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The behaviors of `split` and `slice` are quite similar, so we simply give one example of each"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ExtractionString(' ', [(0,0);(0,0)]), ExtractionString(' string ', [(15,22);(22,22)])]\n",
      "[ExtractionString(' ', [(0,0);(0,0)]), ExtractionString(' string ', [(15,22);(22,22)])]\n"
     ]
    }
   ],
   "source": [
    "extracsts = extracst4.split((0,1, \n",
    "                             2,8, \n",
    "                             len(extracst4)-12,len(extracst4)),\n",
    "                            remove_empty=False)\n",
    "print(list(extracsts))\n",
    "extracsts = extracst4.split((0,1, \n",
    "                             2,8, \n",
    "                             len(extracst4)-12,len(extracst4)),\n",
    "                            remove_empty=True)\n",
    "print(list(extracsts))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remark how the `ExtractionString` containing only a `subtoksep` handles its length: by keeping two empty `intervals`, which ensure its length is still the one of the `subtoksep`. One more time this can not be done using `len(subtoksep)>1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "EvenSizedSortedSet[(15,22);(22,22)]"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "extracsts = extracst4.split((0,1, \n",
    "                             2,8, \n",
    "                             len(extracst4)-12,len(extracst4)),\n",
    "                            remove_empty=True)\n",
    "extracsts[1].intervals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "simp\n",
      "ple \n",
      " str\n",
      "ring\n",
      "g il\n",
      "llus\n",
      "stra\n",
      "atio\n"
     ]
    }
   ],
   "source": [
    "extracsts = extracst4.slice(start=2,stop=None,\n",
    "                            size=4,step=3)\n",
    "for e in extracsts: print(e)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Last modification Tue Jan  3 16:54:05 2023\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "print(\"Last modification {}\".format(datetime.now().strftime(\"%c\")))"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
