NonOverlappingPositions class
=============================

.. automodule:: extractionstring.non_overlapping_positions
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
