sorting module
==============

.. automodule:: extractionstring.sorting
   :members:
   :undoc-members:
   :private-members:
   :special-members:
   :show-inheritance:
