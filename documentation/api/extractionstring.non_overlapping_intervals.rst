NonOverlappingIntervals class
=============================

.. automodule:: extractionstring.non_overlapping_intervals
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
