extractionstring package
========================

Submodules
----------

.. toctree::
   :maxdepth: 4

   extractionstring.even_sized_sorted_set
   extractionstring.extraction_string
   extractionstring.mergesort
   extractionstring.non_overlapping_intervals
   extractionstring.non_overlapping_positions
   extractionstring.sorting

Module contents
---------------

.. automodule:: extractionstring
   :members:
   :show-inheritance:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
