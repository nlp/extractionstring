# Change along the versions

Each main version comes with its own documentation, that is available as a [Jupyter-book](https://jupyterbook.org) that you can consult by opening `documentation/_build/html/index.html` on a web-browser. To generate the documentation, you need to extract the tagged versions on the [official repository](https://framagit.org/nlp/tokenspan), and to construct the corresponding documentation using `jupyter-book build documentation/`

## 0.7- 

Versions prior to 0.7 were not released in this package. They in fact correspond to the old `ExtractionString` objects, developped in the (now deprecated) package available on [framagit:nlp/tokenspan](https://framagit.org/nlp/tokenspan).

After version 0.7 the original `EvenSizedSortedSet` and `ExtarctionString` objects were released, and their changes are commented below.

## 0.8+

Creation of the `EvenSizedSortedSet` and `ExtarctionString` objects.

